package com.example.librarydemoalzira.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.librarydemoalzira.model.LoanDetails;
import com.example.librarydemoalzira.model.LoanDetailsId;

@Repository
public interface LoanDetailsRepository extends JpaRepository<LoanDetails, LoanDetailsId>{

}
