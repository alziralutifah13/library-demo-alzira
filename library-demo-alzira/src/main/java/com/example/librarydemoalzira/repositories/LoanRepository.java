package com.example.librarydemoalzira.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.librarydemoalzira.model.Loan;

@Repository
public interface LoanRepository extends JpaRepository<Loan, Long>{

}
