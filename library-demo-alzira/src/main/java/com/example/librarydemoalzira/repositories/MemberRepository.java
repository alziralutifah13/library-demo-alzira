package com.example.librarydemoalzira.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.librarydemoalzira.model.Member;

@Repository
public interface MemberRepository extends JpaRepository<Member, Long> {

}
