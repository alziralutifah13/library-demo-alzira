package com.example.librarydemoalzira.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.librarydemoalzira.model.Book;

@Repository
public interface BookRepository extends JpaRepository<Book, Long>{

}
