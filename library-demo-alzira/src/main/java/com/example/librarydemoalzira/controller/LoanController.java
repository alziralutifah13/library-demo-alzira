package com.example.librarydemoalzira.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.librarydemoalzira.dto.LoanDTO;
import com.example.librarydemoalzira.dto.LoanDetailsDTO;
import com.example.librarydemoalzira.model.Admin;
import com.example.librarydemoalzira.model.Loan;
import com.example.librarydemoalzira.model.Book;
import com.example.librarydemoalzira.model.LoanDetails;
import com.example.librarydemoalzira.model.Member;
import com.example.librarydemoalzira.repositories.AdminRepository;
import com.example.librarydemoalzira.repositories.BookRepository;
import com.example.librarydemoalzira.repositories.LoanDetailsRepository;
import com.example.librarydemoalzira.repositories.LoanRepository;
import com.example.librarydemoalzira.repositories.MemberRepository;

@RestController
@RequestMapping("/api/loan")
public class LoanController {
	@Autowired
	LoanRepository loanRepo;
	@Autowired
	MemberRepository memberRepo;
	@Autowired
	AdminRepository adminRepo;
	@Autowired
	BookRepository bookRepo;
	@Autowired
	LoanDetailsRepository loanDetailsRepo;
	
	ModelMapper mapper = new ModelMapper();
	
	@PostMapping("/create")
	public ResponseEntity<Object> createLoan(@Valid @RequestBody Loan body) {
		HashMap<String, Object> result = new HashMap<>();
		String message = "Create Loan Success!!";
		HttpStatus status = HttpStatus.OK;
		
		try {
			Member entityMember = memberRepo.findById(body.getMember().getMemberId()).orElse(null);
			Admin entityAdmin = adminRepo.findById(body.getAdmin().getAdminId()).orElse(null);
			
			if (entityMember == null || entityAdmin == null) {
				message = "ERROR, ID tidak Ditemukan!!!";
				status = HttpStatus.NOT_FOUND;
				
				result.put("status", status);
				result.put("message", message);
				
				return ResponseEntity.status(status).body(result);
			}

			Loan saveLoan = loanRepo.save(body);
			LoanDTO entityLoan = mapper.map(saveLoan, LoanDTO.class);
			
			result.put("message", message);
			result.put("status", status.value());
			result.put("data", entityLoan);
			
			return ResponseEntity.status(status).body(result);
		} catch (Exception e) {
			result.put("message", "Failed to create Loan");
			result.put("error", e);
			
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(result);
		}
	}

	@GetMapping("/getall")
	public ResponseEntity<Object> getAllLoan() {
		HashMap<String, Object> result = new HashMap<>();
		String message = "Find All Loan Success!!";
		HttpStatus status = HttpStatus.OK;
		
		try {
			List<Loan> listAllLoan = loanRepo.findAll();
			List<LoanDTO> listLoanDTO = new ArrayList<LoanDTO>();		
			if (listLoanDTO.isEmpty()) {
				status = HttpStatus.NOT_FOUND;
				message = "Loan is Empty";
				
				result.put("status", status.value());
                result.put("message", message);
                return ResponseEntity.status(status).body(result);
			}					
			for (Loan entityLoan : listAllLoan) {
				LoanDTO dtoLoan = mapper.map(entityLoan, LoanDTO.class);
		
				listLoanDTO.add(dtoLoan);
			}			
				result.put("status", status.value());
				result.put("message", message);
				result.put("data", listLoanDTO);
				result.put("total", listLoanDTO.size());
				return ResponseEntity.status(status).body(result);	
		} catch (Exception e) {
			result.put("message", "Failed to get Loan");
            result.put("error", e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(result);
		}		
	}
	
	@GetMapping("/get")
	public ResponseEntity<Object> getLoanById(@Valid @RequestParam(name = "id") Long LoanID) {
		HashMap<String, Object> result = new HashMap<>();
		String message = "Find Loan by Id Succes!!";
		HttpStatus status = HttpStatus.OK;
		
		try {
			Loan entityLoan = loanRepo.findById(LoanID).orElse(null);
			
			 if (entityLoan == null) {
    			status = HttpStatus.NOT_FOUND;
                message = "Loan id : "+ LoanID + " not found!!!";
                
                result.put("status", status);
                result.put("message", message);

                return ResponseEntity.status(status).body(result);
	         }

		 	LoanDTO dtoLoan = mapper.map(entityLoan, LoanDTO.class);
			List<LoanDetailsDTO> listLoanDetailsDto = new ArrayList<LoanDetailsDTO>();
			 for (LoanDetails entityLoanDetails : entityLoan.getLoanDetails()) {	
				LoanDetailsDTO dto = mapper.map(entityLoanDetails, LoanDetailsDTO.class);
			
				listLoanDetailsDto.add(dto);
			 }
			 
		 	dtoLoan.setLoanDetails(listLoanDetailsDto);
		 	
			result.put("status",  status.value());
            result.put("message", message);
            result.put("data", dtoLoan);
            result.put("list loan_details", listLoanDetailsDto);
			return ResponseEntity.status(status).body(result);
		} catch (Exception e) {
			result.put("message", "Failed to get Loan");
            result.put("error", e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(result);
		}	
	}
	
	@PutMapping("/update/{id}")
	public ResponseEntity<Object> updateLoan(@Valid @PathVariable(value = "id") Long loanId, @Valid @RequestBody Loan LoanUpdate) {
		HashMap<String, Object> result = new HashMap<>();
		String message = "Update Loan Success!!";
		HttpStatus status = HttpStatus.OK;
		
		try {
			Loan loan = loanRepo.findById(loanId).orElse(null);
			
			 if (loan == null) {
    			status = HttpStatus.NOT_FOUND;
                message = "Loan id : "+ loanId + " not found!!!";
                
                result.put("status", status);
                result.put("message", message);

                return ResponseEntity.status(status).body(result);
	         }
			 loan.setLoanDate(LoanUpdate.getLoanDate());
			 loan.setActualReturnDate(LoanUpdate.getActualReturnDate());
			 loan.setReturnDate(LoanUpdate.getReturnDate());
			 loan.setLateChargerPrice(LoanUpdate.getLateChargerPrice());
			 loanRepo.save(loan);
			 LoanDTO dtoLoan = mapper.map(loan, LoanDTO.class);
			 
             result.put("status",  status.value());
             result.put("message", message);
             result.put("data", dtoLoan);
             return ResponseEntity.status(status).body(result);	      
		} catch (Exception e) {
			result.put("message", "Failed to update Loan");
            result.put("error", e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(result);
		}	
		
	}
	
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<Object> deleteLoan(@Valid @PathVariable(value = "id") Long LoanID) {
		HashMap<String, Object> result = new HashMap<>();
		String message = "Delete Loan Success!!";
		HttpStatus status = HttpStatus.OK;
		
		try {
			Loan loan = loanRepo.findById(LoanID).orElse(null);
			
			 if (loan == null) {
    			status = HttpStatus.NOT_FOUND;
                message = "Loan id : "+ LoanID + " not found!!!";
                
                result.put("status", status);
                result.put("message", message);

                return ResponseEntity.status(status).body(result);
	         }
	            
			loanRepo.delete(loan);
            result.put("status",  status.value());
            result.put("message", message);

            return ResponseEntity.status(status).body(result);	
	            
		} catch (Exception e) {
			result.put("message", "Failed to delete Loan");
            result.put("error", e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(result);
		}	
	}
	
	@PostMapping("/loan-book")
	public ResponseEntity<Object> loanBook(@Valid @RequestBody Loan loan) {
		HashMap<String, Object> result = new HashMap<>();
	    String message = "Loan Book Success!!";
	    HttpStatus status = HttpStatus.OK;
	    
	    try {
			Member member = memberRepo.findById(loan.getMember().getMemberId()).orElse(null);
			Admin admin = adminRepo.findById(loan.getAdmin().getAdminId()).orElse(null);
			
			if (member == null || admin == null) {
    			status = HttpStatus.NOT_FOUND;
                message = "Id not found!!!";
                
                result.put("status", status);
                result.put("message", message);
                return ResponseEntity.status(status).body(result);
	         }
			
			
			List<LoanDetails> loanDetails = loan.getLoanDetails();
			for (LoanDetails data : loanDetails) {
                Book book = bookRepo.findById(data.getLoanDetailsId().getbookId()).orElse(null);
                    if (book == null) {
                    	return new ResponseEntity<>("Book Id not found!", HttpStatus.NOT_FOUND);       
                     }
                    if (book.getQuantityBook() < data.getQuantityBook()) {
                        return new ResponseEntity<>("Not Enough Book Quantity!", HttpStatus.BAD_REQUEST);
                    } 
            }
			
			Loan saveLoan = loanRepo.save(loan);
			
			for (LoanDetails data : loanDetails) {
				data.getLoanDetailsId().setLoanId(saveLoan.getLoanId());
			}
			
			for (LoanDetails data : loanDetails) {
					Book book = bookRepo.findById(data.getLoanDetailsId().getbookId()).orElse(null);
					
				    book.setQuantityBook(book.getQuantityBook() - data.getQuantityBook());
				    
				    data.setLoanPricePerDay(book.getLoanPriceBook().multiply(new BigDecimal(data.getQuantityBook())));
				    
				    loanDetailsRepo.save(data);
				}
			
			result.put("message", message);
	        result.put("status", status.value());
	        return ResponseEntity.status(status).body(result);
		} catch (Exception e) {
			result.put("message", "Failed to create Loan Book");
	        result.put("error", e);

	        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(result);
		}		
	}
	
	@PostMapping("/return-book")
	public ResponseEntity<Object> returnBook(@Valid @RequestBody Loan loanReturn) {
		HashMap<String, Object> result = new HashMap<>();
	    String message = "Return Book Success!!";
		HttpStatus status = HttpStatus.OK;
		 
		 try {
			 Loan loan = loanRepo.findById(loanReturn.getLoanId()).orElse(null);
			 if (loan == null) {
		            status = HttpStatus.NOT_FOUND;
		            message = "Loan id not found!!!";
		            result.put("status", status);
		            result.put("message", message);
		            return ResponseEntity.status(status).body(result);
		        }
			 loan.setActualReturnDate(loanReturn.getActualReturnDate());
	           	              
	         // calculate late charge price
	         Long timeDifference = loan.getActualReturnDate().getTime() - loan.getReturnDate().getTime();
	         Long daysDifference = calculateDays(timeDifference);
	         BigDecimal lateChargePrice = calculateLateChargePrice(loan, daysDifference);
	         System.out.println(daysDifference);
	         System.out.println(lateChargePrice);
	         // update loan and books
	         for (LoanDetails loanDetails : loan.getLoanDetails()) {
	             Book book = loanDetails.getBook();
	             book.setQuantityBook(book.getQuantityBook() + loanDetails.getQuantityBook());
	             bookRepo.save(book);
	         }

	         loan.setLateChargerPrice(lateChargePrice);
	         loanRepo.save(loan);
                
	         result.put("status", status); 
	         result.put("message", message);
	         return ResponseEntity.status(status).body(result);
		} catch (Exception e) {
			result.put("message", "Failed to return Loan Book");
	        result.put("error", e);

	        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(result);
		}	 
	}
	   
	public Long calculateDays(Long time) {
	        return (time / (1000 * 60 * 60 * 24)) % 365;
	}

	public BigDecimal calculateLateChargePrice(Loan loan, Long daysDifference) {
	    BigDecimal lateChargeRate = new BigDecimal(0);
	    if (daysDifference <= 0) {
	        lateChargeRate = BigDecimal.valueOf(0);
	    } else if (daysDifference <= 1) {
	        lateChargeRate = BigDecimal.valueOf(0.3);
	    } else if (daysDifference <= 3) {
	        lateChargeRate = BigDecimal.valueOf(0.5);
	    } else {
	        lateChargeRate = BigDecimal.valueOf(0.75);
	    }

	    BigDecimal lateChargePrice = new BigDecimal(0);
	    for (LoanDetails loanDetails : loan.getLoanDetails()) {
	        lateChargePrice = lateChargePrice.add(loanDetails.getLoanPricePerDay().multiply(BigDecimal.valueOf(daysDifference)));
	    }

	    lateChargePrice = lateChargePrice.multiply(lateChargeRate);
	    return lateChargePrice;
	}
	
}
