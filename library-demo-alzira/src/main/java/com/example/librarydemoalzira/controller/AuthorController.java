package com.example.librarydemoalzira.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.librarydemoalzira.dto.AuthorDTO;
import com.example.librarydemoalzira.dto.BookDTO;
import com.example.librarydemoalzira.model.Author;
import com.example.librarydemoalzira.model.Book;
import com.example.librarydemoalzira.repositories.AuthorRepository;


@RestController
@RequestMapping("/api/author")
public class AuthorController {
	@Autowired
	AuthorRepository authorRepo;
	
	ModelMapper mapper = new ModelMapper();
	
	@PostMapping("/create")
	public ResponseEntity<Object> createAuthor(@Valid @RequestBody Author body) {
		HashMap<String, Object> result = new HashMap<>();
		String message = "Create Author Success!!";
		HttpStatus status = HttpStatus.OK;
		
		try {
			authorRepo.save(body);
			AuthorDTO authorDTO = mapper.map(body, AuthorDTO.class);
            result.put("status", status);
            result.put("message", message);
            result.put("data", authorDTO);
			
			return ResponseEntity.status(status).body(result);
		} catch (Exception e) {
			result.put("message", "Failed to create Author");
			result.put("error", e);
			
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(result);
		}
	}
	
	@GetMapping("/getall")
	public ResponseEntity<Object> getAllAuthor() {
		HashMap<String, Object> result = new HashMap<>();
		String message = "Find All Author Success!!";
		HttpStatus status = HttpStatus.OK;
		
		try {
			List<Author> listAllAuthor = authorRepo.findAll();
			List<AuthorDTO> listAuthorDTO = new ArrayList<AuthorDTO>();
			if (listAuthorDTO.isEmpty()) {
				status = HttpStatus.NOT_FOUND;
				message = "Author is Empty";
				
				result.put("status", status);
				result.put("message", message);
				result.put("data", listAuthorDTO);
				
				return ResponseEntity.status(status).body(result);
			}

			for (Author entityAuthor : listAllAuthor) {
				AuthorDTO authorDTO = mapper.map(entityAuthor, AuthorDTO.class);
				
				listAuthorDTO.add(authorDTO);
			}
			
			result.put("status", status.value());
			result.put("message", message);
			result.put("data", listAuthorDTO);
			result.put("total", listAuthorDTO.size());
			return ResponseEntity.status(status).body(result);		
		} catch (Exception e) {
			result.put("message", "Failed to get Author");
            result.put("error", e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(result);
		}		
	}
	
	@GetMapping("/get")
	public ResponseEntity<Object> getAuthorById(@Valid @RequestParam(name = "id") Long authorId) {
		HashMap<String, Object> result = new HashMap<>();
		String message = "Find Author by Id Succes!!";
		HttpStatus status = HttpStatus.OK;
		
		try {
			Author entityAuthor = authorRepo.findById(authorId).orElse(null);
			
			 if (entityAuthor == null) {
    			status = HttpStatus.NOT_FOUND;
                message = "Author id : "+ authorId + " not found!!!";
                
                result.put("status", status);
                result.put("message", message);

                return ResponseEntity.status(status).body(result);
	         }
				 AuthorDTO authorDTO = mapper.map(entityAuthor, AuthorDTO.class);
					
				 List<BookDTO> listBookDto = new ArrayList<BookDTO>();
				 for (Book entityBook : entityAuthor.getBooks()) {	
					BookDTO dtoBook = mapper.map(entityBook, BookDTO.class);
				
					listBookDto.add(dtoBook);
				 }
				 
			 	authorDTO.setBooks(listBookDto);
			
	    		result.put("status",  status.value());
	            result.put("message", message);
	            result.put("data", authorDTO);
	            result.put("list Book", listBookDto);
		            
	            return ResponseEntity.status(status).body(result);	
	            
		} catch (Exception e) {
			result.put("message", "Failed to get Author");
            result.put("error", e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(result);
		}		
	}
	
	@PutMapping("/update/{id}")
	public ResponseEntity<Object> updateAuthor(@Valid @PathVariable(value = "id") Long authorId, @Valid @RequestBody Author authorUpdate) {
		HashMap<String, Object> result = new HashMap<>();
		String message = "Update Author Success!!";
		HttpStatus status = HttpStatus.OK;
		
		try {
			Author author = authorRepo.findById(authorId).orElse(null);
			
			 if (author == null) {
    			status = HttpStatus.NOT_FOUND;
                message = "Author id : "+ authorId + " not found!!!";
                
                result.put("status", status);
                result.put("message", message);

                return ResponseEntity.status(status).body(result);
	         }
	  
            author.setAuthorName(authorUpdate.getAuthorName());
            author.setGender(authorUpdate.getGender());
            author.setAge(authorUpdate.getAge());
            author.setCountry(authorUpdate.getCountry());
            author.setRating(authorUpdate.getRating());
            authorRepo.save(author);
            AuthorDTO authorDTO = mapper.map(author, AuthorDTO.class);
            result.put("status",  status.value());
            result.put("message", message);
            result.put("data", authorDTO);
            return ResponseEntity.status(status).body(result);	
	            
		} catch (Exception e) {
			result.put("message", "Failed to update Author");
            result.put("error", e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(result);
		}	
		
	}
	
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<Object> deleteAuthor(@Valid @PathVariable(value = "id") Long authorId) {
		HashMap<String, Object> result = new HashMap<>();
		String message = "Delete Author Success!!";
		HttpStatus status = HttpStatus.OK;
		
		try {
			Author author = authorRepo.findById(authorId).orElse(null);
			
			 if (author == null) {
    			status = HttpStatus.NOT_FOUND;
                message = "Author id : "+ authorId + " not found!!!";
                
                result.put("status", status);
                result.put("message", message);

                return ResponseEntity.status(status).body(result);
	         }
	            
            authorRepo.delete(author);
            result.put("status",  status.value());
            result.put("message", message);

            return ResponseEntity.status(status).body(result);	
	            
		} catch (Exception e) {
			result.put("message", "Failed to delete Author");
            result.put("error", e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(result);
		}	
	}
	
}
