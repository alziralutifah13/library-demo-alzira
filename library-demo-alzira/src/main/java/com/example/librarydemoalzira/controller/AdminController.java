package com.example.librarydemoalzira.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.librarydemoalzira.dto.AdminDTO;
import com.example.librarydemoalzira.dto.LoanDTO;
import com.example.librarydemoalzira.model.Admin;
import com.example.librarydemoalzira.model.Loan;
import com.example.librarydemoalzira.repositories.AdminRepository;

@RestController
@RequestMapping("/api/admin")
public class AdminController {
	@Autowired
	AdminRepository adminRepo;
	
	ModelMapper mapper = new ModelMapper();
	
	@PostMapping("/create")
	public ResponseEntity<Object> createAdmin(@Valid @RequestBody Admin body) {
		HashMap<String, Object> result = new HashMap<>();
		String message = "Create Admin Success!!";
		HttpStatus status = HttpStatus.OK;
		
		try {
			adminRepo.save(body);
			AdminDTO entity = mapper.map(body, AdminDTO.class);
			
			result.put("message", message);
			result.put("status", status.value());
			result.put("data", entity);
			
			return ResponseEntity.status(status).body(result);
		} catch (Exception e) {
			result.put("message", "Failed to create Admin");
			result.put("error", e);
			
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(result);
		}
	}
	
	@GetMapping("/getall")
	public ResponseEntity<Object> getAllAdmin() {
		HashMap<String, Object> result = new HashMap<>();
		String message = "Find All Admin Success!!";
		HttpStatus status = HttpStatus.OK;
		
		try {
			List<Admin> listAll = adminRepo.findAll();
			List<AdminDTO> listDTO = new ArrayList<AdminDTO>();
			if (listDTO.isEmpty()) {
				status = HttpStatus.NOT_FOUND;
				message = "Admin is Empty";
				
				result.put("status", status);
				result.put("message", message);
				result.put("data", listDTO);
				
				return ResponseEntity.status(status).body(result);
			}
			for (Admin entity : listAll) {
				AdminDTO dtoAdmin = mapper.map(entity, AdminDTO.class);
				
				listDTO.add(dtoAdmin);
			}
			result.put("status", status.value());
			result.put("message", message);
			result.put("data", listDTO);
			result.put("total", listDTO.size());
			return ResponseEntity.status(status).body(result);
			
		} catch (Exception e) {
			result.put("message", "Failed to get Admin");
            result.put("error", e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(result);
		}		
	}
	
	@GetMapping("/get")
	public ResponseEntity<Object> getAdminById(@Valid @RequestParam(name = "id") Long adminId) {
		HashMap<String, Object> result = new HashMap<>();
		String message = "Find Admin by Id Succes!!";
		HttpStatus status = HttpStatus.OK;
		
		try {
			Admin entity = adminRepo.findById(adminId).orElse(null);
			
			 if (entity == null) {
    			status = HttpStatus.NOT_FOUND;
                message = "Admin id : "+ adminId + " not found!!!";
                
                result.put("status", status);
                result.put("message", message);

                return ResponseEntity.status(status).body(result);
	         }
			 AdminDTO dtoAdmin = mapper.map(entity, AdminDTO.class);
			 List<LoanDTO> listDTO = new ArrayList<LoanDTO>();
			 for (Loan entityLoan : entity.getLoan()) {
				 LoanDTO dtoLoan= mapper.map(entityLoan, LoanDTO.class);
				
				listDTO.add(dtoLoan);
			 }
			 dtoAdmin.setLoan(listDTO);
				
	    		result.put("status",  status.value());
	            result.put("message", message);
	            result.put("data", dtoAdmin);
	            result.put("list loan", listDTO);
	            return ResponseEntity.status(status).body(result);	
	            
		} catch (Exception e) {
			result.put("message", "Failed to get Admin");
            result.put("error", e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(result);
		}		
	}
	
	@PutMapping("/update/{id}")
	public ResponseEntity<Object> updateAdmin(@Valid @PathVariable(value = "id") Long adminId, @Valid @RequestBody Admin adminUpdate) {
		HashMap<String, Object> result = new HashMap<>();
		String message = "Update Admin Success!!";
		HttpStatus status = HttpStatus.OK;
		
		try {
			Admin admin = adminRepo.findById(adminId).orElse(null);
			
			 if (admin == null) {
    			status = HttpStatus.NOT_FOUND;
                message = "Admin id : "+ adminId + " not found!!!";
                
                result.put("status", status);
                result.put("message", message);

                return ResponseEntity.status(status).body(result);
	         }
			admin.setAdminName(adminUpdate.getAdminName());
			admin.setAddress(adminUpdate.getAddress());
			admin.setPhone(adminUpdate.getPhone());
            adminRepo.save(admin);
            AdminDTO dtoAdmin = mapper.map(admin, AdminDTO.class);
            
            result.put("status",  status.value());
            result.put("message", message);
            result.put("data", dtoAdmin);
            
            return ResponseEntity.status(status).body(result);	
	            
		} catch (Exception e) {
			result.put("message", "Failed to update Admin");
            result.put("error", e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(result);
		}	
		
	}
	
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<Object> deleteAdmin(@Valid @PathVariable(value = "id") Long adminId) {
		HashMap<String, Object> result = new HashMap<>();
		String message = "Delete Admin Success!!";
		HttpStatus status = HttpStatus.OK;
		
		try {
			Admin admin = adminRepo.findById(adminId).orElse(null);
			
			 if (admin == null) {
    			status = HttpStatus.NOT_FOUND;
                message = "Author id : "+ adminId + " not found!!!";
                
                result.put("status", status);
                result.put("message", message);

                return ResponseEntity.status(status).body(result);
	         }
	            
			adminRepo.delete(admin);
            result.put("status",  status.value());
            result.put("message", message);

            return ResponseEntity.status(status).body(result);	
	            
		} catch (Exception e) {
			result.put("message", "Failed to delete Admin");
            result.put("error", e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(result);
		}	
	}
	
}
