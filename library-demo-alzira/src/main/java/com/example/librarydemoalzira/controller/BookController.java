package com.example.librarydemoalzira.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.librarydemoalzira.dto.BookDTO;
import com.example.librarydemoalzira.dto.LoanDetailsDTO;
import com.example.librarydemoalzira.model.Author;
import com.example.librarydemoalzira.model.Book;
import com.example.librarydemoalzira.model.LoanDetails;
import com.example.librarydemoalzira.model.LoanDetailsId;
import com.example.librarydemoalzira.model.Publisher;
import com.example.librarydemoalzira.repositories.AuthorRepository;
import com.example.librarydemoalzira.repositories.BookRepository;
import com.example.librarydemoalzira.repositories.PublisherRepository;

@RestController
@RequestMapping("/api/book")
public class BookController {
	@Autowired
	BookRepository bookRepo;
	@Autowired
	AuthorRepository authorRepo;
	@Autowired
	PublisherRepository publisherRepo;
	
	ModelMapper mapper = new ModelMapper();
	
	@PostMapping("/create")
	public ResponseEntity<Object> createBook(@Valid @RequestBody Book body) {
		HashMap<String, Object> result = new HashMap<>();
		String message = "Create Book Success!!";
		HttpStatus status = HttpStatus.OK;
		
		try {
			Author entityAuthor = authorRepo.findById(body.getAuthor().getAuthorId()).orElse(null);
			Publisher entityPublisher = publisherRepo.findById(body.getPublisher().getpublisherId()).orElse(null);
			
			if (entityAuthor == null || entityPublisher == null) {
				message = "ERROR, ID tidak Ditemukan!!!";
				status = HttpStatus.NOT_FOUND;
				
				result.put("status", status);
				result.put("message", message);
				
				return ResponseEntity.status(status).body(result);
			}
			Book saveBook = bookRepo.save(body);
			BookDTO entityBook = mapper.map(saveBook, BookDTO.class);
			
			result.put("message", message);
			result.put("status", status.value());
			result.put("data", entityBook);
			
			return ResponseEntity.status(status).body(result);
		} catch (Exception e) {
			result.put("message", "Failed to create Author");
			result.put("error", e);
			
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(result);
		}
	}

	@GetMapping("/getall")
	public ResponseEntity<Object> getAllBook() {
		HashMap<String, Object> result = new HashMap<>();
		String message = "Find All Book Success!!";
		HttpStatus status = HttpStatus.OK;
		
		try {
			List<Book> listAllBook = bookRepo.findAll();
			List<BookDTO> listBookDTO = new ArrayList<BookDTO>();
			if (listBookDTO.isEmpty()) {
				status = HttpStatus.NOT_FOUND;
				message = "Book is Empty";
				
				result.put("status", status);
				result.put("message", message);
				result.put("data", listBookDTO);
				
				return ResponseEntity.status(status).body(result);
			}
			for (Book entityBook : listAllBook) {
				BookDTO dtoBook = mapper.map(entityBook, BookDTO.class);
		
				listBookDTO.add(dtoBook);
			}
			result.put("status", status.value());
			result.put("message", message);
			result.put("data", listBookDTO);
			result.put("total", listBookDTO.size());
			return ResponseEntity.status(status).body(result);
		} catch (Exception e) {
			result.put("message", "Failed to get Book");
            result.put("error", e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(result);
		}		
	}
	
	@GetMapping("/get")
	public ResponseEntity<Object> getBookById(@Valid @RequestParam(name = "id") Long bookID) {
		HashMap<String, Object> result = new HashMap<>();
		String message = "Find Book by Id Succes!!";
		HttpStatus status = HttpStatus.OK;
		
		try {
			Book entityBook = bookRepo.findById(bookID).orElse(null);
			
			 if (entityBook == null) {
    			status = HttpStatus.NOT_FOUND;
                message = "Book id : "+ bookID + " not found!!!";
                
                result.put("status", status);
                result.put("message", message);

                return ResponseEntity.status(status).body(result);
	         }
			 
		 	BookDTO dtoBook = mapper.map(entityBook, BookDTO.class);
			List<LoanDetailsDTO> listLoanDetailsDto = new ArrayList<LoanDetailsDTO>();
			 for (LoanDetails entityLoanDetails : entityBook.getLoanDetails()) {	
				LoanDetailsDTO dto = mapper.map(entityLoanDetails, LoanDetailsDTO.class);
			
				listLoanDetailsDto.add(dto);
			 }
			 
		 	dtoBook.setLoanDetails(listLoanDetailsDto);
		 	
			result.put("status",  status.value());
            result.put("message", message);
            result.put("data", dtoBook);
            result.put("list loan_details", listLoanDetailsDto);
			return ResponseEntity.status(status).body(result);
		} catch (Exception e) {
			result.put("message", "Failed to get Book");
            result.put("error", e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(result);
		}	
	}
	
	@PutMapping("/update/{id}")
	public ResponseEntity<Object> updateBook(@Valid @PathVariable(value = "id") Long bookID, @Valid @RequestBody BookDTO bookDTO) {
		HashMap<String, Object> result = new HashMap<>();
		String message = "Update Book Success!!";
		HttpStatus status = HttpStatus.OK;
		
		try {	 
			Optional<Book> book = bookRepo.findById(bookID);
			if (!book.isPresent()) {
				return ResponseEntity.notFound().build();
			}
			 Book bookToUpdate = book.get();
			 mapper.map(bookDTO, bookToUpdate);

             result.put("status",  status.value());
             result.put("message", message);
             result.put("data", bookToUpdate);
             return ResponseEntity.status(status).body(result);	      
		} catch (Exception e) {
			result.put("message", "Failed to update Book");
            result.put("error", e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(result);
		}	
		
	}
	
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<Object> deleteBook(@Valid @PathVariable(value = "id") Long bookID) {
		HashMap<String, Object> result = new HashMap<>();
		String message = "Delete Book Success!!";
		HttpStatus status = HttpStatus.OK;
		
		try {
			Book book = bookRepo.findById(bookID).orElse(null);
			
			 if (book == null) {
    			status = HttpStatus.NOT_FOUND;
                message = "Author id : "+ bookID + " not found!!!";
                
                result.put("status", status);
                result.put("message", message);

                return ResponseEntity.status(status).body(result);
	         }
	            
			bookRepo.delete(book);
            result.put("status",  status.value());
            result.put("message", message);

            return ResponseEntity.status(status).body(result);	
	            
		} catch (Exception e) {
			result.put("message", "Failed to delete Book");
            result.put("error", e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(result);
		}	
	}
	
}
