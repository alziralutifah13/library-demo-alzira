package com.example.librarydemoalzira.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.librarydemoalzira.dto.LoanDetailsDTO;
import com.example.librarydemoalzira.model.Book;
import com.example.librarydemoalzira.model.Loan;
import com.example.librarydemoalzira.model.LoanDetails;
import com.example.librarydemoalzira.model.LoanDetailsId;
import com.example.librarydemoalzira.repositories.BookRepository;
import com.example.librarydemoalzira.repositories.LoanDetailsRepository;
import com.example.librarydemoalzira.repositories.LoanRepository;


@RestController
@RequestMapping("/api/loan-details")
public class LoanDetailsController {
	@Autowired
	LoanDetailsRepository loanDetailsRepo;
	@Autowired
	LoanRepository loanRepo;
	@Autowired
	BookRepository bookRepo;
	
	ModelMapper mapper = new ModelMapper();
	
	@PostMapping("/create")
	public ResponseEntity<Object> createLoanDetails(@Valid @RequestBody LoanDetails body) {
		HashMap<String, Object> result = new HashMap<>();
		String message = "Create LoanDetails Success!!";
		HttpStatus status = HttpStatus.OK;
		
		try {	
			Loan entityLoan = loanRepo.findById(body.getLoanDetailsId().getLoanId()).orElse(null);
			Book entityBook = bookRepo.findById(body.getLoanDetailsId().getbookId()).orElse(null);
			
			if (entityLoan == null || entityBook == null) {
				message = "ERROR, ID tidak Ditemukan!!!";
				status = HttpStatus.NOT_FOUND;
				
				result.put("status", status);
				result.put("message", message);
				
				return ResponseEntity.status(status).body(result);
			}
		
			body.setLoanPricePerDay(calculateLoanPricePerDay(entityBook, body.getQuantityBook()));
            LoanDetails entity = loanDetailsRepo.save(body);
            body.setLoan(entityLoan);
            body.setBook(entityBook);
            body.setLoanDetailsId(entity.getLoanDetailsId());
            
            LoanDetailsDTO dto = mapper.map(body, LoanDetailsDTO.class);
			
			result.put("message", message);
			result.put("status", status.value());
			result.put("data", dto);	
			return ResponseEntity.status(status).body(result);
		} catch (Exception e) {
			result.put("message", "Failed to create LoanDetails");
			result.put("error", e);
			
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(result);
		}
	}
	
	@GetMapping("/getall")
	public ResponseEntity<Object> getAllLoanDetails() {
		HashMap<String, Object> result = new HashMap<>();
		String message = "Find All LoanDetails Success!!";
		HttpStatus status = HttpStatus.OK;
		
		try {
			List<LoanDetails> listAll = loanDetailsRepo.findAll();
			List<LoanDetailsDTO> listDTO = new ArrayList<LoanDetailsDTO>();
			if (listDTO.isEmpty()) {
				status = HttpStatus.NOT_FOUND;
				message = "LoanDetails is Empty";
				
				result.put("status", status);
				result.put("message", message);
				result.put("data", listDTO);
				
				return ResponseEntity.status(status).body(result);
			}
			for (LoanDetails entity : listAll) {
				LoanDetailsDTO dto = mapper.map(entity, LoanDetailsDTO.class);
		
				listDTO.add(dto);
			}
			result.put("status", status.value());
			result.put("message", message);
			result.put("data", listDTO);
			result.put("total", listDTO.size());
			return ResponseEntity.status(status).body(result);			
		} catch (Exception e) {
			result.put("message", "Failed to get LoanDetails");
            result.put("error", e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(result);
		}		
	}
	
	@GetMapping("/get")
	public ResponseEntity<Object> getLoanDetailsById(@Valid @RequestBody LoanDetailsId loanDetailsId) {
		HashMap<String, Object> result = new HashMap<>();
		String message = "Find LoanDetails by Id Succes!!";
		HttpStatus status = HttpStatus.OK;
		
		try {
			LoanDetails entity = loanDetailsRepo.findById(loanDetailsId).orElse(null);
			
			 if (entity == null) {
    			status = HttpStatus.NOT_FOUND;
                message = "LoanDetails id : "+ loanDetailsId + " not found!!!";
                
                result.put("status", status);
                result.put("message", message);

                return ResponseEntity.status(status).body(result);
	         }
			 	LoanDetailsDTO dto = mapper.map(entity, LoanDetailsDTO.class);
				 
				result.put("status",  status.value());
	            result.put("message", message);
	            result.put("data", dto);
	            
				return ResponseEntity.status(status).body(result);
		} catch (Exception e) {
			result.put("message", "Failed to get LoanDetails");
            result.put("error", e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(result);
		}	
	}
	
	@PutMapping("/update/{loanId}/{bookId}")
	public ResponseEntity<Object> updateLoanDetails(@Valid @PathVariable(value = "loanId") Long loanId, @PathVariable(value = "bookId") Long bookId, @Valid @RequestBody LoanDetailsDTO loanDetailsDTO) {
		HashMap<String, Object> result = new HashMap<>();
		String message = "Update LoanDetails Success!!";
		HttpStatus status = HttpStatus.OK;
		
		try {
			Optional<LoanDetails> loanDetails = loanDetailsRepo.findById(new LoanDetailsId(loanId, bookId));
			
			if (!loanDetails.isPresent()) {
				return ResponseEntity.notFound().build();
			}
			LoanDetails loanDetailsToUpdate = loanDetails.get();
			mapper.map(loanDetailsDTO, loanDetailsToUpdate);
			LoanDetails updatedLoanDetails = loanDetailsRepo.save(loanDetailsToUpdate);
			LoanDetailsDTO updatedLoanDetailsDTO = mapper.map(updatedLoanDetails, LoanDetailsDTO.class);
			 
             result.put("status",  status.value());
             result.put("message", message);
             result.put("data", updatedLoanDetailsDTO);
             return ResponseEntity.status(status).body(result);	
	            
		} catch (Exception e) {
			result.put("message", "Failed to update LoanDetails");
            result.put("error", e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(result);
		}	
		
	}
	
	@DeleteMapping("/delete/{loanId}/{bookId}")
	public ResponseEntity<Object> deleteLoanDetails(@Valid @PathVariable(value = "loanId") Long loanId, @PathVariable(value = "bookId") Long bookId) {
	    HashMap<String, Object> result = new HashMap<>();
	    String message = "Delete LoanDetails Success!!";
	    HttpStatus status = HttpStatus.OK;

	    try {
	    	Optional<LoanDetails> loanDetails = loanDetailsRepo.findById(new LoanDetailsId(loanId, bookId));
	    	if (!loanDetails.isPresent()) {
	    		return ResponseEntity.notFound().build();
	    	}
	    	loanDetailsRepo.delete(loanDetails.get());
	    	
            result.put("status",  status.value());
            result.put("message", message);

            return ResponseEntity.status(status).body(result);	
	    } catch (Exception e) {
	        result.put("message", "Failed to delete LoanDetails");
	        result.put("error", e.getMessage());
	        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(result);
	    }
	}
	
	public BigDecimal calculateLoanPricePerDay(Book book, int quantity) {
	    return book.getLoanPriceBook().multiply(BigDecimal.valueOf(quantity));
	}

	
}
