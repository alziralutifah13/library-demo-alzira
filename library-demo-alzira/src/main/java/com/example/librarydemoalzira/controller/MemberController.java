package com.example.librarydemoalzira.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.librarydemoalzira.dto.LoanDTO;
import com.example.librarydemoalzira.dto.MemberDTO;
import com.example.librarydemoalzira.model.Loan;
import com.example.librarydemoalzira.model.Member;
import com.example.librarydemoalzira.repositories.MemberRepository;

@RestController
@RequestMapping("/api/member")
public class MemberController {
	@Autowired
	MemberRepository memberRepo;
	
ModelMapper mapper = new ModelMapper();
	
	@PostMapping("/create")
	public ResponseEntity<Object> createMember(@Valid @RequestBody Member body) {
		HashMap<String, Object> result = new HashMap<>();
		String message = "Create Member Success!!";
		HttpStatus status = HttpStatus.OK;
		
		try {
			memberRepo.save(body);
			MemberDTO entity = mapper.map(body, MemberDTO.class);
			
			result.put("message", message);
			result.put("status", status.value());
			result.put("data", entity);
			
			return ResponseEntity.status(status).body(result);
		} catch (Exception e) {
			result.put("message", "Failed to create Admin");
			result.put("error", e);
			
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(result);
		}
	}
	
	@GetMapping("/getall")
	public ResponseEntity<Object> getAllMember() {
		HashMap<String, Object> result = new HashMap<>();
		String message = "Find All Member Success!!";
		HttpStatus status = HttpStatus.OK;
		
		try {
			List<Member> listAll = memberRepo.findAll();
			List<MemberDTO> listDTO = new ArrayList<MemberDTO>();
			if (listDTO.isEmpty()) {
				status = HttpStatus.NOT_FOUND;
				message = "Member is Empty";
				
				result.put("status", status);
				result.put("message", message);
				result.put("data", listDTO);
				
				return ResponseEntity.status(status).body(result);
			}
			for (Member entity : listAll) {
				MemberDTO dtoMember = mapper.map(entity, MemberDTO.class);
				
				listDTO.add(dtoMember);
			}
			result.put("status", status.value());
			result.put("message", message);
			result.put("data", listDTO);
			result.put("total", listDTO.size());
			return ResponseEntity.status(status).body(result);
		} catch (Exception e) {
			result.put("message", "Failed to get Member");
            result.put("error", e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(result);
		}		
	}
	
	@GetMapping("/get")
	public ResponseEntity<Object> getMemberById(@Valid @RequestParam(name = "id") Long memberId) {
		HashMap<String, Object> result = new HashMap<>();
		String message = "Find Member by Id Succes!!";
		HttpStatus status = HttpStatus.OK;
		
		try {
			Member entity = memberRepo.findById(memberId).orElse(null);
			
			 if (entity == null) {
    			status = HttpStatus.NOT_FOUND;
                message = "Member id : "+ memberId + " not found!!!";
                
                result.put("status", status);
                result.put("message", message);

                return ResponseEntity.status(status).body(result);
	         }
			 MemberDTO dtoMember = mapper.map(entity, MemberDTO.class);
				
			 List<LoanDTO> listDTO = new ArrayList<LoanDTO>();
			 for (Loan entityLoan : entity.getLoan()) {
				 LoanDTO dtoLoan= mapper.map(entityLoan, LoanDTO.class);
				
				listDTO.add(dtoLoan);
			 }
			 dtoMember.setLoan(listDTO);
				
    		 result.put("status",  status.value());
             result.put("message", message);
             result.put("data", dtoMember);
             result.put("list loan", listDTO);
             return ResponseEntity.status(status).body(result);	
	            
		} catch (Exception e) {
			result.put("message", "Failed to get Member");
            result.put("error", e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(result);
		}		
	}
	
	@PutMapping("/update/{id}")
	public ResponseEntity<Object> updateAdmin(@Valid @PathVariable(value = "id") Long memberId, @Valid @RequestBody Member memberUpdate) {
		HashMap<String, Object> result = new HashMap<>();
		String message = "Update Member Success!!";
		HttpStatus status = HttpStatus.OK;
		
		try {
			Member member = memberRepo.findById(memberId).orElse(null);
			
			 if (member == null) {
    			status = HttpStatus.NOT_FOUND;
                message = "Member id : "+ memberId + " not found!!!";
                
                result.put("status", status);
                result.put("message", message);

                return ResponseEntity.status(status).body(result);
	         }
			 member.setMemberName(memberUpdate.getMemberName());
			 member.setAddress(memberUpdate.getAddress());
			 member.setPhone(memberUpdate.getPhone());
			
             memberRepo.save(member);
             MemberDTO dtoMember = mapper.map(member, MemberDTO.class);
            
             result.put("status",  status.value());
             result.put("message", message);
             result.put("data", dtoMember);
            
             return ResponseEntity.status(status).body(result);	
	            
		} catch (Exception e) {
			result.put("message", "Failed to update Member");
            result.put("error", e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(result);
		}	
		
	}
	
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<Object> deleteMember(@Valid @PathVariable(value = "id") Long memberId) {
		HashMap<String, Object> result = new HashMap<>();
		String message = "Delete Member Success!!";
		HttpStatus status = HttpStatus.OK;
		
		try {
			Member member = memberRepo.findById(memberId).orElse(null);
			
			 if (member == null) {
    			status = HttpStatus.NOT_FOUND;
                message = "Member id : "+ memberId + " not found!!!";
                
                result.put("status", status);
                result.put("message", message);

                return ResponseEntity.status(status).body(result);
	         }
	            
			 memberRepo.delete(member);
             result.put("status",  status.value());
             result.put("message", message);

             return ResponseEntity.status(status).body(result);	
	            
		} catch (Exception e) {
			result.put("message", "Failed to delete Member");
            result.put("error", e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(result);
		}	
	}
	
	
}
