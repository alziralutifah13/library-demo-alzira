package com.example.librarydemoalzira.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "book")
public class Book implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "book_id")
	private Long bookId;
	
	@Column(nullable = false)
	private String bookTitle;
	
	@ManyToOne
	@JoinColumn(name = "publisher_id")
	private Publisher publisher;
	
	@ManyToOne
    @JoinColumn(name = "author_id")
    private Author author;
	
	@Column(nullable = false)
	@Temporal(TemporalType.DATE)
	private Date releaseDate;
	
	@Column(nullable = false)
	private int quantityBook;
	
	@Column(nullable = false)
	private BigDecimal loanPriceBook;
	
	@OneToMany(mappedBy = "book")
	private List<LoanDetails> loanDetails;

	public Book() {
	}

	public Book(Long bookId, String bookTitle, Publisher publisher, Author author, Date releaseDate, int quantityBook,
			BigDecimal loanPriceBook, List<LoanDetails> loanDetails) {
		super();
		this.bookId = bookId;
		this.bookTitle = bookTitle;
		this.publisher = publisher;
		this.author = author;
		this.releaseDate = releaseDate;
		this.quantityBook = quantityBook;
		this.loanPriceBook = loanPriceBook;
		this.loanDetails = loanDetails;
	}

	public Long getbookId() {
		return bookId;
	}

	public void setbookId(Long bookId) {
		this.bookId = bookId;
	}

	public String getBookTitle() {
		return bookTitle;
	}

	public void setBookTitle(String bookTitle) {
		this.bookTitle = bookTitle;
	}

	public Publisher getPublisher() {
		return publisher;
	}

	public void setPublisher(Publisher publisher) {
		this.publisher = publisher;
	}

	public Author getAuthor() {
		return author;
	}

	public void setAuthor(Author author) {
		this.author = author;
	}

	public Date getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}

	public int getQuantityBook() {
		return quantityBook;
	}

	public void setQuantityBook(int quantityBook) {
		this.quantityBook = quantityBook;
	}

	public BigDecimal getLoanPriceBook() {
		return loanPriceBook;
	}

	public void setLoanPriceBook(BigDecimal loanPriceBook) {
		this.loanPriceBook = loanPriceBook;
	}

	
	public List<LoanDetails> getLoanDetails() {
		return loanDetails;
	}

	public void setLoanDetails(List<LoanDetails> loanDetails) {
		this.loanDetails = loanDetails;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((author == null) ? 0 : author.hashCode());
		result = prime * result + ((bookId == null) ? 0 : bookId.hashCode());
		result = prime * result + ((bookTitle == null) ? 0 : bookTitle.hashCode());
		result = prime * result + ((loanDetails == null) ? 0 : loanDetails.hashCode());
		result = prime * result + ((loanPriceBook == null) ? 0 : loanPriceBook.hashCode());
		result = prime * result + ((publisher == null) ? 0 : publisher.hashCode());
		result = prime * result + quantityBook;
		result = prime * result + ((releaseDate == null) ? 0 : releaseDate.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Book other = (Book) obj;
		if (author == null) {
			if (other.author != null)
				return false;
		} else if (!author.equals(other.author))
			return false;
		if (bookId == null) {
			if (other.bookId != null)
				return false;
		} else if (!bookId.equals(other.bookId))
			return false;
		if (bookTitle == null) {
			if (other.bookTitle != null)
				return false;
		} else if (!bookTitle.equals(other.bookTitle))
			return false;
		if (loanDetails == null) {
			if (other.loanDetails != null)
				return false;
		} else if (!loanDetails.equals(other.loanDetails))
			return false;
		if (loanPriceBook == null) {
			if (other.loanPriceBook != null)
				return false;
		} else if (!loanPriceBook.equals(other.loanPriceBook))
			return false;
		if (publisher == null) {
			if (other.publisher != null)
				return false;
		} else if (!publisher.equals(other.publisher))
			return false;
		if (quantityBook != other.quantityBook)
			return false;
		if (releaseDate == null) {
			if (other.releaseDate != null)
				return false;
		} else if (!releaseDate.equals(other.releaseDate))
			return false;
		return true;
	}
	
}
