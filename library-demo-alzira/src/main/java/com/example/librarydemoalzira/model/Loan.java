package com.example.librarydemoalzira.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "loan")
public class Loan implements Serializable{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "loan_id")
	private Long loanId;
	
	@ManyToOne
	@JoinColumn(name = "member_id")
	private Member member;
	
	@ManyToOne
	@JoinColumn(name = "admin_id")
	private Admin admin;
	
	@Column(nullable = true)
	@Temporal(TemporalType.DATE)
	private Date loanDate;
	
	@Column(nullable = true)
	@Temporal(TemporalType.DATE)
	private Date returnDate;
	
	@Column(nullable = true)
	@Temporal(TemporalType.DATE)
	private Date actualReturnDate;
	
	@Column(nullable = true)
	private BigDecimal lateChargerPrice;
	
	@OneToMany(mappedBy = "loan")
	private List<LoanDetails> loanDetails;

	public Loan() {
	}

	public Loan(Long loanId, Member member, Admin admin, Date loanDate, Date returnDate, Date actualReturnDate,
			BigDecimal lateChargerPrice, List<LoanDetails> loanDetails) {
		super();
		this.loanId = loanId;
		this.member = member;
		this.admin = admin;
		this.loanDate = loanDate;
		this.returnDate = returnDate;
		this.actualReturnDate = actualReturnDate;
		this.lateChargerPrice = lateChargerPrice;
		this.loanDetails = loanDetails;
	}

	public Long getLoanId() {
		return loanId;
	}

	public void setLoanId(Long loanId) {
		this.loanId = loanId;
	}

	public Member getMember() {
		return member;
	}

	public void setMember(Member member) {
		this.member = member;
	}

	public Admin getAdmin() {
		return admin;
	}

	public void setAdmin(Admin admin) {
		this.admin = admin;
	}

	public Date getLoanDate() {
		return loanDate;
	}

	public void setLoanDate(Date loanDate) {
		this.loanDate = loanDate;
	}

	public Date getReturnDate() {
		return returnDate;
	}

	public void setReturnDate(Date returnDate) {
		this.returnDate = returnDate;
	}

	public Date getActualReturnDate() {
		return actualReturnDate;
	}

	public void setActualReturnDate(Date actualReturnDate) {
		this.actualReturnDate = actualReturnDate;
	}

	public BigDecimal getLateChargerPrice() {
		return lateChargerPrice;
	}

	public void setLateChargerPrice(BigDecimal lateChargerPrice) {
		this.lateChargerPrice = lateChargerPrice;
	}

	public List<LoanDetails> getLoanDetails() {
		return loanDetails;
	}

	public void setLoanDetails(List<LoanDetails> loanDetails) {
		this.loanDetails = loanDetails;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((actualReturnDate == null) ? 0 : actualReturnDate.hashCode());
		result = prime * result + ((admin == null) ? 0 : admin.hashCode());
		result = prime * result + ((lateChargerPrice == null) ? 0 : lateChargerPrice.hashCode());
		result = prime * result + ((loanDate == null) ? 0 : loanDate.hashCode());
		result = prime * result + ((loanDetails == null) ? 0 : loanDetails.hashCode());
		result = prime * result + ((loanId == null) ? 0 : loanId.hashCode());
		result = prime * result + ((member == null) ? 0 : member.hashCode());
		result = prime * result + ((returnDate == null) ? 0 : returnDate.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Loan other = (Loan) obj;
		if (actualReturnDate == null) {
			if (other.actualReturnDate != null)
				return false;
		} else if (!actualReturnDate.equals(other.actualReturnDate))
			return false;
		if (admin == null) {
			if (other.admin != null)
				return false;
		} else if (!admin.equals(other.admin))
			return false;
		if (lateChargerPrice == null) {
			if (other.lateChargerPrice != null)
				return false;
		} else if (!lateChargerPrice.equals(other.lateChargerPrice))
			return false;
		if (loanDate == null) {
			if (other.loanDate != null)
				return false;
		} else if (!loanDate.equals(other.loanDate))
			return false;
		if (loanDetails == null) {
			if (other.loanDetails != null)
				return false;
		} else if (!loanDetails.equals(other.loanDetails))
			return false;
		if (loanId == null) {
			if (other.loanId != null)
				return false;
		} else if (!loanId.equals(other.loanId))
			return false;
		if (member == null) {
			if (other.member != null)
				return false;
		} else if (!member.equals(other.member))
			return false;
		if (returnDate == null) {
			if (other.returnDate != null)
				return false;
		} else if (!returnDate.equals(other.returnDate))
			return false;
		return true;
	}
	
	
}
