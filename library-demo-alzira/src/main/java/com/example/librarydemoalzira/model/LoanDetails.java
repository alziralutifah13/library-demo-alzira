package com.example.librarydemoalzira.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

@Entity
@Table(name = "loan_details")
public class LoanDetails {
	
	@EmbeddedId
	private LoanDetailsId loanDetailsId;
	
	@ManyToOne
	@MapsId("loan_id")
	@JoinColumn(name = "loan_id")
	private Loan loan;
	
	@ManyToOne
	@MapsId("book_id")
	@JoinColumn(name = "book_id")
	private Book book;
	
	@Column(nullable = false)
	private int quantityBook;
	
	@Column(nullable = false)
	private BigDecimal loanPricePerDay;

	public LoanDetails() {
	}

	public LoanDetails(LoanDetailsId loanDetailsId, Loan loan, Book book, int quantityBook,
			BigDecimal loanPricePerDay) {
		this.loanDetailsId = loanDetailsId;
		this.loan = loan;
		this.book = book;
		this.quantityBook = quantityBook;
		this.loanPricePerDay = loanPricePerDay;
	}

	public LoanDetailsId getLoanDetailsId() {
		return loanDetailsId;
	}

	public void setLoanDetailsId(LoanDetailsId loanDetailsId) {
		this.loanDetailsId = loanDetailsId;
	}

	public Loan getLoan() {
		return loan;
	}

	public void setLoan(Loan loan) {
		this.loan = loan;
	}

	public Book getBook() {
		return book;
	}

	public void setBook(Book book) {
		this.book = book;
	}

	public int getQuantityBook() {
		return quantityBook;
	}

	public void setQuantityBook(int quantityBook) {
		this.quantityBook = quantityBook;
	}

	public BigDecimal getLoanPricePerDay() {
		return loanPricePerDay;
	}

	public void setLoanPricePerDay(BigDecimal loanPricePerDay) {
		this.loanPricePerDay = loanPricePerDay;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((book == null) ? 0 : book.hashCode());
		result = prime * result + ((loan == null) ? 0 : loan.hashCode());
		result = prime * result + ((loanDetailsId == null) ? 0 : loanDetailsId.hashCode());
		result = prime * result + ((loanPricePerDay == null) ? 0 : loanPricePerDay.hashCode());
		result = prime * result + quantityBook;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LoanDetails other = (LoanDetails) obj;
		if (book == null) {
			if (other.book != null)
				return false;
		} else if (!book.equals(other.book))
			return false;
		if (loan == null) {
			if (other.loan != null)
				return false;
		} else if (!loan.equals(other.loan))
			return false;
		if (loanDetailsId == null) {
			if (other.loanDetailsId != null)
				return false;
		} else if (!loanDetailsId.equals(other.loanDetailsId))
			return false;
		if (loanPricePerDay == null) {
			if (other.loanPricePerDay != null)
				return false;
		} else if (!loanPricePerDay.equals(other.loanPricePerDay))
			return false;
		if (quantityBook != other.quantityBook)
			return false;
		return true;
	}
}
