package com.example.librarydemoalzira.dto;

import java.math.BigDecimal;

import com.example.librarydemoalzira.model.LoanDetailsId;

public class LoanDetailsDTO {
	private LoanDetailsId loanDetailsId;
	private LoanDTO loan;
	private BookDTO book;
	private int quantityBook;
	private BigDecimal loanPricePerDay;
	
	public LoanDetailsDTO() {
	}

	public LoanDetailsDTO(LoanDetailsId loanDetailsId, LoanDTO loan, BookDTO book, int quantityBook,
			BigDecimal loanPricePerDay) {
		super();
		this.loanDetailsId = loanDetailsId;
		this.loan = loan;
		this.book = book;
		this.quantityBook = quantityBook;
		this.loanPricePerDay = loanPricePerDay;
	}

	public LoanDetailsId getLoanDetailsId() {
		return loanDetailsId;
	}

	public void setLoanDetailsId(LoanDetailsId loanDetailsId) {
		this.loanDetailsId = loanDetailsId;
	}

	public LoanDTO getLoan() {
		return loan;
	}

	public void setLoan(LoanDTO loan) {
		this.loan = loan;
	}

	public BookDTO getBook() {
		return book;
	}

	public void setBook(BookDTO book) {
		this.book = book;
	}

	public int getQuantityBook() {
		return quantityBook;
	}

	public void setQuantityBook(int quantityBook) {
		this.quantityBook = quantityBook;
	}

	public BigDecimal getLoanPricePerDay() {
		return loanPricePerDay;
	}

	public void setLoanPricePerDay(BigDecimal loanPricePerDay) {
		this.loanPricePerDay = loanPricePerDay;
	}

}
