package com.example.librarydemoalzira.dto;

import java.util.List;

public class AdminDTO {
	private Long adminId;
	private String adminName;
	private String address;
	private String phone;
    private List<LoanDTO> loan;
    
	public AdminDTO() {
	}

	public AdminDTO(Long adminId, String adminName, String address, String phone, List<LoanDTO> loan) {
		super();
		this.adminId = adminId;
		this.adminName = adminName;
		this.address = address;
		this.phone = phone;
		this.loan = loan;
	}

	public Long getAdminId() {
		return adminId;
	}

	public void setAdminId(Long adminId) {
		this.adminId = adminId;
	}

	public String getAdminName() {
		return adminName;
	}

	public void setAdminName(String adminName) {
		this.adminName = adminName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public List<LoanDTO> getLoan() {
		return loan;
	}

	public void setLoan(List<LoanDTO> loan) {
		this.loan = loan;
	}
    
}
