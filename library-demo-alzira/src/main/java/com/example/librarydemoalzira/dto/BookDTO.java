package com.example.librarydemoalzira.dto;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


public class BookDTO {
	private Long bookId;
	private String bookTitle;
	private PublisherDTO publisher;
	private AuthorDTO author;
	private Date releaseDate;
	private int quantityBook;
	private BigDecimal loanPriceBook;
	private List<LoanDetailsDTO> loanDetails;
	
	public BookDTO() {

	}

	public BookDTO(Long bookId, String bookTitle, PublisherDTO publisher, AuthorDTO author, Date releaseDate,
			int quantityBook, BigDecimal loanPriceBook, List<LoanDetailsDTO> loanDetails) {
		super();
		this.bookId = bookId;
		this.bookTitle = bookTitle;
		this.publisher = publisher;
		this.author = author;
		this.releaseDate = releaseDate;
		this.quantityBook = quantityBook;
		this.loanPriceBook = loanPriceBook;
		this.loanDetails = loanDetails;
	}

	public Long getbookId() {
		return bookId;
	}

	public void setbookId(Long bookId) {
		this.bookId = bookId;
	}

	public String getBookTitle() {
		return bookTitle;
	}

	public void setBookTitle(String bookTitle) {
		this.bookTitle = bookTitle;
	}

	public PublisherDTO getPublisher() {
		return publisher;
	}

	public void setPublisher(PublisherDTO publisher) {
		this.publisher = publisher;
	}

	public AuthorDTO getAuthor() {
		return author;
	}

	public void setAuthor(AuthorDTO author) {
		this.author = author;
	}

	public Date getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}

	public int getQuantityBook() {
		return quantityBook;
	}

	public void setQuantityBook(int quantityBook) {
		this.quantityBook = quantityBook;
	}

	public BigDecimal getLoanPriceBook() {
		return loanPriceBook;
	}

	public void setLoanPriceBook(BigDecimal loanPriceBook) {
		this.loanPriceBook = loanPriceBook;
	}

	public List<LoanDetailsDTO> getLoanDetails() {
		return loanDetails;
	}

	public void setLoanDetails(List<LoanDetailsDTO> loanDetails) {
		this.loanDetails = loanDetails;
	}
	
	
}
