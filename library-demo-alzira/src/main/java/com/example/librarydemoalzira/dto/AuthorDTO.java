package com.example.librarydemoalzira.dto;

import java.util.List;


public class AuthorDTO {
	private Long authorId;
	private String authorName;
	private String gender;
	private Integer age;
	private String country;
	private String rating;
	private List<BookDTO> books;
	
	public AuthorDTO() {
	}

	public AuthorDTO(Long authorId, String authorName, String gender, Integer age, String country, String rating,
			List<BookDTO> books) {
		super();
		this.authorId = authorId;
		this.authorName = authorName;
		this.gender = gender;
		this.age = age;
		this.country = country;
		this.rating = rating;
		this.books = books;
	}



	public Long getAuthorId() {
		return authorId;
	}

	public void setAuthorId(Long authorId) {
		this.authorId = authorId;
	}

	public String getAuthorName() {
		return authorName;
	}

	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getRating() {
		return rating;
	}

	public void setRating(String rating) {
		this.rating = rating;
	}

	public List<BookDTO> getBooks() {
		return books;
	}

	public void setBooks(List<BookDTO> books) {
		this.books = books;
	}

}
