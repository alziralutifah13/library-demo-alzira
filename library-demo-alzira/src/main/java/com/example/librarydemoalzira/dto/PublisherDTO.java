package com.example.librarydemoalzira.dto;

import java.util.List;

import com.example.librarydemoalzira.model.Book;

public class PublisherDTO {
	private Long publisherId;
	private String publisherName;
	private List<BookDTO> books;
	
	public PublisherDTO() {
	}

	public PublisherDTO(Long publisherId, String publisherName, List<BookDTO> books) {
		super();
		this.publisherId = publisherId;
		this.publisherName = publisherName;
		this.books = books;
	}

	public Long getpublisherId() {
		return publisherId;
	}

	public void setpublisherId(Long publisherId) {
		this.publisherId = publisherId;
	}

	public String getPublisherName() {
		return publisherName;
	}

	public void setPublisherName(String publisherName) {
		this.publisherName = publisherName;
	}

	public List<BookDTO> getBooks() {
		return books;
	}

	public void setBooks(List<BookDTO> books) {
		this.books = books;
	}


}
