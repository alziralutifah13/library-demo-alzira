package com.example.librarydemoalzira.dto;

import java.util.List;


public class MemberDTO {
	private Long memberId;
	private String memberName;
	private String address;
	private String phone;
	private List<LoanDTO> loan;
	
	public MemberDTO() {
		
	}
	
	public MemberDTO(Long memberId, String memberName, String address, String phone, List<LoanDTO> loan) {
		super();
		this.memberId = memberId;
		this.memberName = memberName;
		this.address = address;
		this.phone = phone;
		this.loan = loan;
	}
	public Long getMemberId() {
		return memberId;
	}

	public void setMemberId(Long memberId) {
		this.memberId = memberId;
	}

	public String getMemberName() {
		return memberName;
	}

	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public List<LoanDTO> getLoan() {
		return loan;
	}

	public void setLoan(List<LoanDTO> loan) {
		this.loan = loan;
	}
}