package com.example.librarydemoalzira;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LibraryDemoAlziraApplication {

	public static void main(String[] args) {
		SpringApplication.run(LibraryDemoAlziraApplication.class, args);
	}

}
